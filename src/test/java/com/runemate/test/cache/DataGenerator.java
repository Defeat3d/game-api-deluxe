package com.runemate.test.cache;

import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.api.hybrid.entities.definitions.*;
import com.runemate.game.cache.*;
import java.io.*;
import org.apache.commons.cli.*;

public class DataGenerator {

    public static void main(String[] args) throws IOException {
        Options options = new Options();
        options.addOption("c", "cache", true, "Cache root");
        options.addOption("d", "directory", true, "Output directory");
        options.addOption(null, "items", false, "Generate items");
        options.addOption(null, "objects", false, "Generate objects");
        options.addOption(null, "npcs", false, "Generate npcs");

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd;
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.err.println("Error parsing command line options: " + e.getMessage());
            System.exit(-1);
            return;
        }

        if (!cmd.hasOption("c")) {
            System.err.println("Cache root not specified");
            System.exit(-1);
            return;
        }

        File cache = new File(cmd.getOptionValue("cache"));
        if (!cache.exists()) {
            System.err.println("Cache does not exist: " + cache);
            System.exit(-1);
            return;
        }

        File directory = new File(cmd.getOptionValue("directory"));
        if (!directory.exists()) {
            directory.mkdirs();
        }

        JS5CacheController.setDefaultCacheController(cache);
        if (options.hasOption("items")) {
            dumpItems(directory);
        }

        if (options.hasOption("objects")) {
            dumpObjects(directory);
        }

        if (options.hasOption("npcs")) {
            dumpNpcs(directory);
        }
    }

    private static void dumpNpcs(final File directory) throws IOException {
        try (DataClass npcs = DataClass.create(directory, "NpcID")) {
            for (NpcDefinition definition : NpcDefinition.loadAll()) {
                npcs.add(definition.getName(), definition.getId());
            }
        }
    }

    private static void dumpObjects(final File directory) throws IOException {
        try (DataClass nonNulls = DataClass.create(directory, "ObjectID");
            DataClass nulls = DataClass.create(directory, "NullObjectID")) {
            for (GameObjectDefinition definition : GameObjectDefinition.loadAll()) {
                if ("null".equals(definition.getName())) {
                    nulls.add(definition.getName(), definition.getId());
                } else {
                    nonNulls.add(definition.getName(), definition.getId());
                }
            }
        }
    }

    private static void dumpItems(final File directory) throws IOException {
        try (DataClass items = DataClass.create(directory, "ItemID")) {
            for (ItemDefinition ext : ItemDefinition.loadAll()) {
                CacheItemDefinition parsed = ((CacheItemDefinition.Extended) ext).getBase();
                items.add(parsed.getName(), parsed.getId());
            }
        }
    }
}

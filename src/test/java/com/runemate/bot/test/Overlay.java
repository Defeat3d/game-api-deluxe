package com.runemate.bot.test;

import com.runemate.client.bind.*;
import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.geom.*;
import com.runemate.game.api.hybrid.input.*;
import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.region.*;
import java.awt.*;
import java.util.*;
import java.util.concurrent.*;
import javax.swing.*;
import javax.swing.Timer;
import lombok.*;
import lombok.extern.log4j.*;

@Log4j2
public class Overlay extends JComponent {

    private final JFrame frame;
    private final FeatureTestBot bot;
    private final Timer timer;

    public Overlay(final FeatureTestBot bot) {
        this.bot = bot;

        frame = new JFrame("Model viewer");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setAlwaysOnTop(true);
        frame.add(this);
        frame.setUndecorated(true);
        frame.setBackground(new Color(0, 0, 0, 0));

        frame.pack();
        frame.setVisible(true);

        timer = new Timer(100, e -> {
            // Code to update the overlay every 50 ms goes here
            try {
                bot.getPlatform().invokeAndWait(() -> {
                    InteractablePoint loc = Screen.getLocation();
                    if (loc != null) {
                        frame.setLocation(loc);
                    }

                    InteractableRectangle bounds = Screen.getBounds();
                    if (bounds != null) {
                        frame.setSize(bounds.getSize());
                    }
                });
            } catch (ExecutionException | InterruptedException ignored) {

            }
            repaint();
        });
        timer.start(); // Start the timer
    }

    public void stop() {
        timer.stop();
        frame.setVisible(false);
        frame.dispose();
    }

    @SneakyThrows
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        // Code to render the overlay goes here
        Graphics2D graphics = (Graphics2D) g;
        graphics.setPaint(Color.RED);
        graphics.drawRect(0, 0, getWidth() - 1, getHeight() - 1);
        bot.getPlatform().invoke(() -> {
            final Player player = Players.getLocal();
            if (player != null) {
                Map<String, Object> cache = new HashMap<>();
                Area.Rectangular area = Scene.getArea();
                Coordinate position = player.getServerPosition();
                position.render(graphics);
                for (int i = 1; i <= 20; i++) {
                    Coordinate d = position.derive(i, i);
                    if (d.minimap().isVisible(area, cache)) {
                        d.render(graphics);
                    }
                }
            }

//            final double minimapZoomSetting;
//            if (Environment.getClientType() == ClientType.RUNELITE) {
//                minimapZoomSetting = OpenRuneLite.getMinimapZoom();
//            } else {
//                minimapZoomSetting = 4d;
//            }
//
//            final int unzoomedRenderDist = 20 << 7;
//            final double zoomRatio = (4d / minimapZoomSetting);
//            final int distance = (int) (unzoomedRenderDist * zoomRatio);

//            final Point mouse = Mouse.getPosition();
//            graphics.drawRect((int) mouse.getX(), (int) mouse.getY(), 2, 2);
        });
    }


}

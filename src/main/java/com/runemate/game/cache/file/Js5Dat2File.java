package com.runemate.game.cache.file;

import com.runemate.game.cache.io.*;
import java.io.*;
import java.nio.*;
import java.nio.channels.*;
import java.nio.file.*;

public class Js5Dat2File extends Js5File {
    private static final int UNIT_SIZE = 520;

    public Js5Dat2File(File file, OpenOption... mode) {
        super(file, mode);
    }

    private static int getAmountOfChunks(FileChannel channel) {
        try {
            return (int) Math.ceil(channel.size() / (double) UNIT_SIZE);
        } catch (IOException e) {
            e.printStackTrace();
            throw new IllegalStateException("Unable to get data file unit count.", e);
        }
    }

    private static void checkRead(FileChannel channel, long index) {
        if (index < 0) {
            throw new BufferUnderflowException();
        }
        if (index + 1 > getAmountOfChunks(channel)) {
            throw new BufferOverflowException();
        }
    }

    int getAmountOfChunks() throws IOException {
        return getAmountOfChunks(getChannel());
    }

    Chunk readChunk(InitialChunkInfo query) throws IOException {
        return readChunk(query.getGroup(), query.getIndex());
    }

    private Chunk readChunk(int groupId, long index) throws IOException {
        if (index <= 0) {
            return null;
        }
        FileChannel channel = getChannel();
        checkRead(channel, index);
        ByteBuffer dataBuffer = ByteBuffer.allocate(UNIT_SIZE);
        //We use a long for index to prevent overflows.
        channel.read(dataBuffer, index * UNIT_SIZE);
        try (Js5InputStream stream = new Js5InputStream(dataBuffer.array())) {
            int catalog_ = groupId <= 0xFFFF ? stream.readUnsignedShort() : stream.readInt();
            int id = stream.readUnsignedShort();
            int nextId = stream.readBytes(3);
            int archiveIndex = stream.readUnsignedByte();
            //log.info(groupId+" catalog="+catalog_+" current="+id+" next="+nextId+" archive="+archiveIndex);
            return new Chunk(
                id,
                nextId,
                stream.getBackingArray(),
                stream.position()
            );
        }
    }

    public class Chunk {
        private final int id;
        private final int successorId;
        private final byte[] contents;
        private final int offset;

        public Chunk(int id, int nextId, byte[] contents, int offset) {
            this.id = id;
            this.successorId = nextId;
            this.contents = contents;
            this.offset = offset;
        }

        public int getId() {
            return id;
        }

        public Chunk getNext(InitialChunkInfo initial) throws IOException {
            return readChunk(initial.getGroup(), successorId);
        }

        public byte[] getContents() {
            return contents;
        }

        public int getOffset() {
            return offset;
        }
    }
}

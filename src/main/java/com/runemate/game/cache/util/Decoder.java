package com.runemate.game.cache.util;

public interface Decoder<E> {
    void decode(E buffer, byte[] data, int[] xteaKeys);
}

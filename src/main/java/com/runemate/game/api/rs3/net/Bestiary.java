package com.runemate.game.api.rs3.net;

import com.runemate.game.api.hybrid.util.collections.*;
import java.util.*;
import org.jetbrains.annotations.*;

@Deprecated
public final class Bestiary {

    private Bestiary() {
    }

    /**
     * Gets the beast with the given id
     */
    @Nullable
    public static Beast lookupById(int id) {
        return null;
    }

    /**
     * Gets a list of all Beast's with the given name
     */
    public static List<Beast> lookupByName(String name) {
        return Collections.emptyList();
    }

    /**
     * Gets a list of all beasts in the given area
     *
     * @param area
     * @return
     */
    public static List<Beast> lookupByArea(String area) {
        return Collections.emptyList();
    }

    /**
     * Gets a list of all beasts within the given slayer category
     */
    public static List<Beast> lookupBySlayerCategory(int slayerCategoryId) {
        return Collections.emptyList();
    }

    /**
     * Gets a list of all beasts within the given slayer category
     */
    public static List<Beast> lookupBySlayerCategory(String slayerCategory) {
        return Collections.emptyList();
    }

    @Deprecated
    public static class Beast {

        public Beast(String json) {
            // no-op
        }

        public String getName() {
            return null;
        }

        public int getId() {
            return 0;
        }

        public String getDescription() {
            return null;
        }

        public String getWeakness() {
            return null;
        }

        public boolean isAttackable() {
            return false;
        }

        public boolean isAggressive() {
            return false;
        }

        public boolean isPoisonous() {
            return false;
        }

        public double getXp() {
            return 0d;
        }

        public int getLifepoints() {
            return 0;
        }

        public int getCombatLevel() {
            return 0;
        }

        public int getDefenceLevel() {
            return 0;
        }

        public int getAttackLevel() {
            return 0;
        }

        public int getMagicLevel() {
            return 0;
        }

        public int getRangedLevel() {
            return 0;
        }

        public int getSlayerLevel() {
            return 0;
        }

        public boolean isMembersOnly() {
            return false;
        }

        public String getSlayerCategory() {
            return null;
        }

        public List<String> getLocations() {
            return Collections.emptyList();
        }

        public PairList<String, Integer> getAnimations() {
            return new PairList<>(Collections.emptyList());
        }

        @Override
        public String toString() {
            return "Beast";
        }
    }
}

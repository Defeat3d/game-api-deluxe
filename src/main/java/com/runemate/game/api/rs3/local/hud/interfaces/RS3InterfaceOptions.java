package com.runemate.game.api.rs3.local.hud.interfaces;

import com.runemate.client.framework.open.*;
import com.runemate.game.api.hybrid.local.*;

@Deprecated
public final class RS3InterfaceOptions {
    private RS3InterfaceOptions() {
    }

    public static boolean isSlimHeadersEnabled() {
        return false;
    }

    public static boolean areTitleBarsHiddenWhenInterfacesLocked() {
        return false;
    }

    public static boolean areInterfacesLocked() {
        return false;
    }
}

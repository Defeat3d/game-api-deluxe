package com.runemate.game.api.osrs.local.hud.interfaces;

import com.runemate.game.api.hybrid.local.*;
import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import com.runemate.game.api.script.*;
import lombok.experimental.*;
import lombok.extern.log4j.*;

@Log4j2
@UtilityClass
public class AutoRetaliate {

    public boolean isActivated() {
        return VarpID.AUTO_RETALIATE.getValue() == 0;
    }

    public boolean activate() {
        log.info("Activating auto retaliate");
        return toggle(true);
    }

    public boolean deactivate() {
        log.info("Deactivating auto retaliate");
        return toggle(false);
    }

    private boolean toggle(final boolean activate) {
        if (isActivated() == activate) {
            return true;
        }
        final InterfaceComponent component = getComponent();
        return component != null
            && ControlPanelTab.COMBAT_OPTIONS.open()
            && component.interact("Auto retaliate")
            && Execution.delayUntil(() -> isActivated() == activate, 800);
    }

    public InterfaceComponent getComponent() {
        return Interfaces.newQuery()
            .containers(593)
            .types(InterfaceComponent.Type.CONTAINER)
            .actions("Auto retaliate")
            .results()
            .first();
    }

}
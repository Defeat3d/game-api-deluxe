package com.runemate.game.api.osrs;

import com.runemate.client.game.open.*;
import java.util.*;

public final class OSRSClientPreferences {

    private OSRSClientPreferences() {
    }


    public static boolean areRoofsHidden() {
        return OpenClientPreferences.isHidingRoofs();
    }


    public static boolean isAudioDisabled() {
        return OpenClientPreferences.isAudioDisabled();
    }


    public static boolean isUsernameHidden() {
        return OpenClientPreferences.isUsernameHidden();
    }


    public static int getProtocolVersion() {
        return OpenClientPreferences.getProtocolVersion();
    }


    public static String getRememberedUsername() {
        return (String) OpenClientPreferences.getRememberedUsername();
    }


    public static Map<Integer, Integer> getRememberedTwoFactorTokens() {
        return OpenClientPreferences.getRememberedTwoFactorTokens();
    }

    @Deprecated
    public static int getRememberedTwoFactorToken(final String username) {
        final Map<Integer, Integer> tokens = getRememberedTwoFactorTokens();
        return tokens.getOrDefault(usernameToKey(username), -1);
    }

    @Deprecated
    public static void putRememberedTwoFactorToken(final String username, final int value) {

    }

    @Deprecated
    public static void setRememberedTwoFactorTokens(final Map<Integer, Integer> tokens) {

    }

    @Deprecated
    public static void putRememberedTwoFactorTokens(final Map<Integer, Integer> tokens) {

    }

    private static int usernameToKey(CharSequence var0) {
        int var2 = var0.length();
        int var3 = 0;

        for (int var4 = 0; var4 < var2; ++var4) {
            var3 = (var3 << 5) - var3 + var0.charAt(var4);
        }

        return var3;
    }
}

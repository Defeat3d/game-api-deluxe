package com.runemate.game.api.osrs.net;

import com.google.gson.*;
import com.google.gson.stream.*;
import com.runemate.io.*;
import java.io.*;
import java.net.*;

/**
 * Used for looking up item price information from the OSBuddy Exchange.
 * @deprecated no longer available
 */
@Deprecated
public final class OSBuddyExchange {
    private static final String API_URL_BASE =
        "https://storage.googleapis.com/osbuddy-exchange/item/%s.json";

    private OSBuddyExchange() {
    }

    public static GuidePrice getGuidePrice(int itemId) {
        try (IOTunnel tunnel = new IOTunnel(new URL(String.format(API_URL_BASE, itemId)).openStream())) {
            String page = new String(tunnel.readAsArray());
            return new GuidePrice(page);
        } catch (IOException e) {
        }
        return null;
    }

    public static final class GuidePrice {
        private final int overall;
        private final int buying;
        private final int buyingQuantity;
        private final int selling;
        private final int sellingQuantity;

        private GuidePrice(String json) {
            JsonReader reader = new JsonReader(new StringReader(json));
            reader.setLenient(true);
            JsonObject base = new JsonParser().parse(reader).getAsJsonObject();
            this.overall = base.getAsJsonPrimitive("overall_average").getAsInt();
            this.buying = base.getAsJsonPrimitive("buy_average").getAsInt();
            this.buyingQuantity = base.getAsJsonPrimitive("buy_quantity").getAsInt();
            this.selling = base.getAsJsonPrimitive("sell_average").getAsInt();
            this.sellingQuantity = base.getAsJsonPrimitive("sell_quantity").getAsInt();
        }

        public int getOverall() {
            return overall;
        }

        public int getBuying() {
            return buying;
        }

        public int getBuyingQuantity() {
            return buyingQuantity;
        }

        public int getSelling() {
            return selling;
        }

        public int getSellingQuantity() {
            return sellingQuantity;
        }
    }
}

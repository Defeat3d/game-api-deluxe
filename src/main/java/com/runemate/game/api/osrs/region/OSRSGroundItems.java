package com.runemate.game.api.osrs.region;

import com.runemate.client.game.open.*;
import com.runemate.commons.internal.scene.entities.objects.*;
import com.runemate.commons.internal.scene.locations.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.osrs.entities.*;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;
import lombok.*;
import org.apache.commons.lang3.*;

public final class OSRSGroundItems {
    private OSRSGroundItems() {
    }

    public static LocatableEntityQueryResults<GroundItem> getLoaded(Predicate<? super GroundItem> filter) {
        Coordinate base = Scene.getBase(0);
        Collection<OpenItemNode> results = OpenItemNode.getLoaded();
        Collection<GroundItem> items = results.stream()
            .map(node -> (GroundItem) new OSRSGroundItem(node, new Coordinate(node.getX() + base.getX(), node.getY() + base.getY(), node.getPlane())))
            .filter(x -> filter == null || filter.test(x))
            .collect(Collectors.toList());

        return new LocatableEntityQueryResults<>(items);
    }


    public static LocatableEntityQueryResults<GroundItem> getLoadedOn(Coordinate position, Predicate<? super GroundItem> filter) {
        Coordinate.SceneOffset offset = position.getSceneOffset();
        TileArea containment = new TileArea(new Tile(offset.getX(), offset.getY(), position.getPlane()));

        Coordinate base = Scene.getBase(0);
        Collection<OpenItemNode> results = OpenItemNode.getLoadedWithin(containment);
        Collection<GroundItem> items = results.stream()
            .map(node -> (GroundItem) new OSRSGroundItem(node, new Coordinate(node.getX() + base.getX(), node.getY() + base.getY(), node.getPlane())))
            .filter(x -> filter == null || filter.test(x))
            .collect(Collectors.toList());

        return new LocatableEntityQueryResults<>(items);
    }

    public static LocatableEntityQueryResults<GroundItem> getLoadedWithin(Area area, Predicate<? super GroundItem> filter) {
        Area.Rectangular rect = area.toRectangular();
        Coordinate.SceneOffset offset = rect.getBottomLeft().getSceneOffset();
        int plane = rect.getBottomLeft().getPlane();
        Coordinate base = Scene.getBase();

        TileArea containment = new TileArea(offset.getX(), offset.getY(), rect.getWidth(), rect.getHeight(), plane);
        Collection<OpenItemNode> results = OpenItemNode.getLoadedWithin(containment);
        Collection<GroundItem> items = results.stream()
            .map(node -> (GroundItem) new OSRSGroundItem(node, new Coordinate(node.getX() + base.getX(), node.getY() + base.getY(), node.getPlane())))
            .filter(x -> filter == null || filter.test(x))
            .collect(Collectors.toList());

        return new LocatableEntityQueryResults<>(items);
    }

    @SneakyThrows
    @Deprecated
    public static LocatableEntityQueryResults<GroundItem> getLoadedOn(
        final Coordinate position,
        long loadedGroundItemsUid,
        final Predicate<? super GroundItem> filter
    ) {
        return getLoadedOn(position, filter);
    }

}

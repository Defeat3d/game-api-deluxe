package com.runemate.game.api.osrs.region;

import com.runemate.client.game.open.*;
import com.runemate.commons.internal.scene.locations.*;
import com.runemate.game.api.hybrid.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.queries.results.*;
import com.runemate.game.api.osrs.entities.*;
import java.io.*;
import java.util.*;
import java.util.function.*;
import java.util.stream.*;

public final class OSRSGameObjects {

    private static final LocatableEntityQueryResults<GameObject> EMPTY =
        new LocatableEntityQueryResults<>(Collections.emptyList(), null);

    private OSRSGameObjects() {
    }


    public static LocatableEntityQueryResults<GameObject> getLoaded(Predicate<GameObject> filter) {
        Coordinate base = Scene.getBase();
        List<GameObject> objects = OpenGameObject.getLoaded().stream()
            .map(object -> {
                Coordinate position = new Coordinate(object.getX(), object.getY(), object.getPlane());
                return new OSRSGameObject(object, position.derive(base.getX(), base.getY()));
            })
            .filter(object -> filter == null || filter.test(object))
            .collect(Collectors.toList());
        return new LocatableEntityQueryResults<>(objects);
    }


    public static LocatableEntityQueryResults<GameObject> getLoadedOn(Coordinate c, Predicate<GameObject> filter) {
        Coordinate base = Scene.getBase();
        TileArea containment = new TileArea(new Tile(c.getX() - base.getX(), c.getY() - base.getY(), c.getPlane()));
        List<GameObject> objects = OpenGameObject.getLoadedWithin(containment).stream()
            .map(object -> {
                Coordinate position = new Coordinate(object.getX(), object.getY(), object.getPlane());
                return new OSRSGameObject(object, position.derive(base.getX(), base.getY()));
            })
            .filter(object -> filter == null || filter.test(object))
            .collect(Collectors.toList());
        return new LocatableEntityQueryResults<>(objects);
    }


    public static LocatableEntityQueryResults<GameObject> getLoadedWithin(Area area, Predicate<GameObject> filter) {
        Area.Rectangular rectangular = area.toRectangular();
        Coordinate bl = rectangular.getBottomLeft();
        Coordinate tr = rectangular.getTopRight();
        Coordinate base = Scene.getBase();
        TileArea containment = new TileArea(
            new Tile(bl.getX() - base.getX(), bl.getY() - base.getY(), bl.getPlane()),
            new Tile(tr.getX() - base.getX(), tr.getY() - base.getY(), tr.getPlane())
        );
        List<GameObject> objects = OpenGameObject.getLoadedWithin(containment).stream()
            .map(object -> {
                Coordinate position = new Coordinate(object.getX(), object.getY(), object.getPlane());
                return new OSRSGameObject(object, position.derive(base.getX(), base.getY()));
            })
            .filter(object -> filter == null || filter.test(object))
            .collect(Collectors.toList());
        return new LocatableEntityQueryResults<>(objects);
    }
}

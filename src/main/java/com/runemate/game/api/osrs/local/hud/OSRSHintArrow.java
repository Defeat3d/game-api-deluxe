package com.runemate.game.api.osrs.local.hud;

import com.runemate.client.game.open.*;
import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.api.osrs.region.*;
import java.awt.*;
import javafx.scene.canvas.*;
import org.jetbrains.annotations.*;

public final class OSRSHintArrow implements HintArrow, Validatable {
    private static final OSRSHintArrow instance = new OSRSHintArrow();

    // 1 = npc
    // 10 = player
    private OSRSHintArrow() {
    }

    public static OSRSHintArrow getInstance() {
        return instance;
    }

    @Nullable

    @Override
    public Coordinate getPosition() {
        int type = OpenHintArrow.getType();
        if (type == 1 || type == 10) {
            Actor target = getTarget(type);
            if (target != null) {
                return target.getPosition();
            } else {
                int x = OpenHintArrow.getX();
                int y = OpenHintArrow.getY();
                if (x > 0 && y > 0) {
                    return new Coordinate(x, y, 0);
                }
            }
        } else if (type >= 2 && type <= 6) {
            int x = OpenHintArrow.getX();
            int y = OpenHintArrow.getY();
            if (x > 0 && y > 0) {
                return new Coordinate(x, y, 0);
            }
        }
        return null;
    }

    @Nullable
    @Override
    public Coordinate.HighPrecision getHighPrecisionPosition() {
        final Coordinate c = getPosition();
        if (c != null) {
            return new Coordinate.HighPrecision(c.getX() << 7, c.getY() << 7, c.getPlane());
        }
        return null;
    }

    @Nullable
    @Override
    public Area.Rectangular getArea() {
        final Coordinate position = getPosition();
        return position != null ? new Area.Rectangular(position) : null;
    }

    @Override
    public int getType() {
        return OpenHintArrow.getType();
    }

    @Override
    public Actor getTarget() {
        return getTarget(OpenHintArrow.getType());
    }

    @Override
    public boolean isValid() {
        return getType() != 0;
    }

    @Override
    public void render(Graphics2D g2d) {
        Renderable renderable = getTarget();
        if (renderable != null || (renderable = getPosition()) != null) {
            renderable.render(g2d);
        }
    }

    @Override
    public void render(GraphicsContext gc) {
        Renderable renderable = getTarget();
        if (renderable != null || (renderable = getPosition()) != null) {
            renderable.render(gc);
        }
    }

    @Override
    public String toString() {
        Actor target = getTarget();
        if (target != null) {
            return "HintArrow{target=" + getTarget() + "}";
        } else {
            Coordinate position = getPosition();
            if (position != null) {
                return "HintArrow{position=(" + position.getX() + ", " + position.getY() + ", " +
                    position.getPlane() + ")}";
            }
        }
        return "HintArrow{type=" + getType() + "}";
    }


    private Actor getTarget(int type) {
        if (type == 1) {
            int index = OpenHintArrow.getNpcIndex();
            if (index >= 0) {
                return OSRSNpcs.getByIndex(index);
            }
        } else if (type == 10) {
            int index = OpenHintArrow.getPlayerIndex();
            if (index >= 0) {
                return OSRSPlayers.getAt(index);
            }
        }
        return null;
    }
}

package com.runemate.game.api.script.framework.listeners;

import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.*;

public interface EquipmentListener extends EventListener {
    /**
     * @deprecated replaced by {@link InventoryListener#onInventoryUpdated(ItemEvent)}
     */
    @Deprecated
    default void onItemEquipped(final ItemEvent event) {
    }

    /**
     * @deprecated replaced by {@link InventoryListener#onInventoryUpdated(ItemEvent)}
     */
    @Deprecated
    default void onItemUnequipped(final ItemEvent event) {
    }
}

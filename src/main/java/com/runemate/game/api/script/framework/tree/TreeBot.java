package com.runemate.game.api.script.framework.tree;

import com.runemate.game.api.script.framework.*;

/**
 * This is an extension of the LoopingBot class in the same way that
 * TaskBot was. This class aims to provide a more robust implementation
 * than TaskBot did by allowing Tasks to be executed on not only a
 * successful validation, but also on a validation failure.
 */
public abstract class TreeBot extends LoopingBot {
    private TreeTask root;

    /**
     * Creates the root task, invoked exactly one time per instance of the bot.
     *
     * @return A non-null TreeTask
     */
    public abstract TreeTask createRootTask();

    /**
     * This is where the magic happens. Performs a sort of in-order
     * traversal of the Task tree based on the validator. When a leaf
     * node is reached, it will be executed instead of validated.
     */
    @Override
    public final void onLoop() {
        if (root == null) {
            root = createRootTask();
            if (root == null) {
                throw new IllegalStateException(
                    "Attempting to create the root task resulted in a null root.");
            }
        }
        TreeTask currentTask = root;
        // Traverse the tree starting at the root in tasks.
        while (!currentTask.isLeaf()) {
            // If the task validates, move on to its success task.
            // Otherwise, move on to its failure task.
            if (currentTask.validate()) {
                TreeTask successTask = currentTask.successTask();
                if (successTask == null) {
                    throw new UnsupportedOperationException(
                        "Branch(" + currentTask.getClass().getName()
                            + ") had a null Success Task.");
                } else {
                    currentTask = successTask;
                }
            } else {
                TreeTask failureTask = currentTask.failureTask();
                if (failureTask == null) {
                    throw new UnsupportedOperationException(
                        "Branch(" + currentTask.getClass().getName()
                            + ") had a null Failure Task.");
                } else {
                    currentTask = failureTask;
                }
            }
        }
        // When a leaf is reached, execute it.
        currentTask.execute();
    }
}

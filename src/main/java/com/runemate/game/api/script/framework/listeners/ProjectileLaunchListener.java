package com.runemate.game.api.script.framework.listeners;

import com.runemate.game.api.script.framework.listeners.events.*;
import java.util.*;

@Deprecated
public interface ProjectileLaunchListener extends EventListener {
    void onProjectileLaunched(ProjectileLaunchEvent event);
}

package com.runemate.game.api.hybrid.entities.attributes;

import lombok.*;
import org.jetbrains.annotations.*;
import javax.annotation.Nullable;
import org.jetbrains.annotations.*;

/**
 * An attribute representing a characteristic.
 */
public final class Attribute {
    private final int id;
    private final Object value;

    public Attribute(final long id, final Object value) {
        this.id = (int) id;
        this.value = value;
    }

    /**
     * Gets the identifier of the attribute
     */
    public long getId() {
        return id;
    }

    /**
     * Gets the value of this characteristic
     */
    @NonNull
    public Object getAsObject() {
        return value;
    }

    /**
     * Gets the value of this attribute and casts it to an Integer
     */
    @Nullable
    public Integer getAsInteger() {
        if (isInteger()) {
            return (Integer) value;
        }
        return null;
    }

    /**
     * Checks whether the attributes value is an Integer
     */
    public boolean isInteger() {
        return value instanceof Integer;
    }

    @Nullable
    public String getAsString() {
        if (isString()) {
            return (String) value;
        }
        return null;
    }

    public boolean isString() {
        return value instanceof String;
    }

    @Nullable
    public AttributeDefinition getDefinition() {
        return AttributeDefinition.load(id);
    }

    @Override
    public String toString() {
        return "Attribute " + id + " (" + (isString() ? getAsString() : getAsInteger()) + ')';
    }
}

package com.runemate.game.api.hybrid.web.vertex.items;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.input.direct.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.web.*;
import com.runemate.game.api.script.*;
import com.runemate.game.api.script.framework.core.*;
import java.util.*;
import java.util.regex.*;
import lombok.extern.log4j.*;

@Log4j2
public class BasicItemTeleportVertex extends ItemTeleportVertex {

    public BasicItemTeleportVertex(final Coordinate position, final Pattern action, final SpriteItemQueryBuilder builder) {
        super(position, action, builder);
    }

    @Override
    public boolean step(final Map<String, Object> cache) {
        final var local = (Player) cache.get(WebPath.AVATAR);
        final var localPos = (Coordinate) cache.get(WebPath.AVATAR_POS);
        if (local == null || localPos == null) {
            return false;
        }

        final var item = getItem();
        if (item == null) {
            log.warn("Failed to resolve target entity for {}", this);
            return false;
        }

        if ((boolean) cache.get(WebPath.DIRECT_INPUT)) {
            var ma = MenuAction.forSpriteItem(item, action);
            if (ma != null) {
                DirectInput.send(ma);
                return Execution.delayUntil(nearDestination(local), () -> local.getAnimationId() != -1, 3000);
            }
        }

        return item.interact(action) && Execution.delayUntil(nearDestination(local), () -> local.getAnimationId() != -1, 3000);
    }

    @Override
    public String toString() {
        final var joiner = new StringJoiner(",", "BasicItemTeleport(", ")");
        joiner.add("position=" + position);
        if (BotPlatform.isBotThread()) {
            var result = getItem();
            joiner.add("item=" + result);
        }
        return joiner.toString();
    }
}

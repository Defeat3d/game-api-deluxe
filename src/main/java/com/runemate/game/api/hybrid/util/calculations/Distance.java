package com.runemate.game.api.hybrid.util.calculations;

import com.runemate.game.api.hybrid.entities.details.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.region.*;
import java.util.*;

public final class Distance {
    private Distance() {
    }

    public static Algorithm getDefaultAlgorithm() {
        return Algorithm.MANHATTAN;
    }

    public static double to(final Locatable destination) {
        return to(destination, getDefaultAlgorithm());
    }

    public static double to(final Locatable destination, final Algorithm algorithm) {
        return to(destination, algorithm, new HashMap<>(1));
    }

    public static double to(final Locatable destination, final Map<String, Object> cache) {
        return to(destination, getDefaultAlgorithm(), cache);
    }

    /**
     * Calculates the distance between the local player and the destination using the specified algorithm.
     *
     * @return The calculated value or -1
     */
    public static double to(
        final Locatable destination, final Algorithm algorithm,
        Map<String, Object> cache
    ) {
        return between(Players.getLocal(), destination, algorithm, cache);
    }

    public static double between(final Locatable start, final Locatable destination) {
        return between(start, destination, getDefaultAlgorithm());
    }

    public static double between(
        final Locatable start, final Locatable destination,
        Map<String, Object> cache
    ) {
        return between(start, destination, getDefaultAlgorithm(), cache);
    }

    /**
     * Calculates the distance between the start and destination using the specified algorithm.
     *
     * @return The calculated value or Double.POSITIVE_INFINITY if the Locatables are on the same plane.
     */
    public static double between(
        final Locatable start, final Locatable destination,
        final Algorithm algorithm
    ) {
        return between(start, destination, algorithm, new HashMap<>(1));
    }

    /**
     * Calculates the distance between the start and destination center coordinates using the specified algorithm.
     *
     * @return The calculated value or Double.POSITIVE_INFINITY if the Locatables are on the same plane.
     */
    public static double between(
        final Locatable start, final Locatable destination,
        final Algorithm algorithm, Map<String, Object> cache
    ) {
        if (start != null && destination != null) {
            Area.Rectangular sarea = start.getArea();
            if (sarea != null) {
                Area.Rectangular darea = destination.getArea();
                if (darea != null) {
                    Coordinate scenter = sarea.getCenter();
                    Coordinate dcenter = darea.getCenter();
                    if (scenter.getPlane() == dcenter.getPlane()) {
                        return algorithm.calculate(scenter.getX(), scenter.getY(), dcenter.getX(),
                            dcenter.getY()
                        );
                    }
                }
            }
        }
        return Double.POSITIVE_INFINITY;
    }

    public static double between(final Coordinate start, final Coordinate destination) {
        return between(start, destination, getDefaultAlgorithm());
    }

    /**
     * Calculates the distance between the start and destination using the specified algorithm.
     *
     * @return The calculated value or Double.POSITIVE_INFINITY if the Locatables are on the same plane.
     */
    public static double between(
        final Coordinate start, final Coordinate destination,
        final Algorithm algorithm
    ) {
        if (start != null && destination != null && start.getPlane() == destination.getPlane()) {
            return algorithm.calculate(start.getX(), start.getY(), destination.getX(),
                destination.getY()
            );
        }
        return Double.POSITIVE_INFINITY;
    }

    /**
     * A variety of algorithms used for calculating distances.
     */
    public enum Algorithm {
        EUCLIDEAN {
            @Override
            public double calculate(
                final double startX, final double startY,
                final double destinationX, final double destinationY
            ) {
                if (startX == destinationX && startY == destinationY) {
                    return 0;
                }
                return StrictMath.sqrt(
                    EUCLIDEAN_SQUARED.calculate(startX, startY, destinationX, destinationY));
            }

            @Override
            public double calculate(
                long startX, long startY, long destinationX,
                long destinationY
            ) {
                if (startX == destinationX && startY == destinationY) {
                    return 0;
                }
                return StrictMath.sqrt(
                    EUCLIDEAN_SQUARED.calculate(startX, startY, destinationX, destinationY));
            }
        },
        EUCLIDEAN_SQUARED {
            @Override
            public double calculate(
                final double startX, final double startY,
                final double destinationX, final double destinationY
            ) {
                if (startX == destinationX && startY == destinationY) {
                    return 0;
                }
                return StrictMath.pow(destinationX - startX, 2) +
                    StrictMath.pow(destinationY - startY, 2);
            }

            @Override
            public double calculate(
                long startX, long startY, long destinationX,
                long destinationY
            ) {
                //Perform one operation at a time so that if an ArithmeticException is thrown we'll be able to deduce the value that caused it.
                long dX = Math.subtractExact(destinationX, startX);
                dX = Math.multiplyExact(dX, dX);
                long dY = Math.subtractExact(destinationY, startY);
                dY = Math.multiplyExact(dY, dY);
                return Math.addExact(dX, dY);
            }
        },
        MANHATTAN {
            @Override
            public double calculate(
                final double startX, final double startY,
                final double destinationX, final double destinationY
            ) {
                if (startX == destinationX && startY == destinationY) {
                    return 0;
                }
                return Math.abs(destinationX - startX) + Math.abs(destinationY - startY);
            }

            @Override
            public double calculate(
                long startX, long startY, long destinationX,
                long destinationY
            ) {
                if (startX == destinationX && startY == destinationY) {
                    return 0;
                }
                return Math.addExact(
                    Math.abs(Math.subtractExact(destinationX, startX)),
                    Math.abs(Math.subtractExact(destinationY, startY))
                );
            }
        },
        CHEBYSHEV {
            @Override
            public double calculate(
                final double startX, final double startY,
                final double destinationX, final double destinationY
            ) {
                return Math.max(Math.abs(destinationX - startX), Math.abs(destinationY - startY));
            }

            @Override
            public double calculate(
                long startX, long startY, long destinationX,
                long destinationY
            ) {
                return Math.max(
                    Math.abs(Math.subtractExact(destinationX, startX)),
                    Math.abs(Math.subtractExact(destinationY, startY))
                );
            }
        };

        public abstract double calculate(
            final double startX, final double startY,
            final double destinationX, final double destinationY
        );

        public abstract double calculate(
            final long startX, final long startY,
            final long destinationX, final long destinationY
        );
    }
}

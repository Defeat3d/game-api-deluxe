package com.runemate.game.api.hybrid.cache.loaders;

import com.runemate.game.api.hybrid.cache.*;
import com.runemate.game.api.hybrid.cache.configs.*;
import com.runemate.game.api.hybrid.cache.elements.*;
import com.runemate.game.cache.item.*;
import java.util.*;

public class InventoryDefinitionLoader extends SerializedFileLoader<CacheInventoryDefinition> {

    public InventoryDefinitionLoader() {
        super(CacheIndex.CONFIGS.getId());
    }

    @Override
    protected CacheInventoryDefinition construct(final int entry, final int file, final Map<String, Object> arguments) {
        return new CacheInventoryDefinition(file);
    }
}

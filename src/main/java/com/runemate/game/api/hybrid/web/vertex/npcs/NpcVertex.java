package com.runemate.game.api.hybrid.web.vertex.npcs;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.queries.*;
import com.runemate.game.api.hybrid.web.*;
import com.runemate.game.api.hybrid.web.vertex.*;
import java.util.*;
import java.util.regex.*;
import lombok.*;
import lombok.extern.log4j.*;
import org.jetbrains.annotations.*;

@Log4j2
public abstract class NpcVertex implements Vertex {

    protected final Coordinate position;
    protected final Pattern action;
    protected final @ToString.Exclude NpcQueryBuilder builder;

    public NpcVertex(final Coordinate position, final Pattern action, final NpcQueryBuilder builder) {
        this.position = position;
        this.action = action;
        this.builder = builder;
    }

    @Nullable
    @Override
    public Coordinate getPosition() {
        return position;
    }

    @Override
    public ScanResult scan(final Map<String, Object> cache) {
        final var object = getNpc();
        if (object == null) {
            return new ScanResult(null, ScanAction.CONTINUE);
        }

        if (object.isVisible()) {
            return new ScanResult(this, ScanAction.STOP);
        }

        final var pos = object.getPosition();
        if (pos == null || !pos.minimap().isVisible()) {
            return new ScanResult(null, ScanAction.CONTINUE);
        }

        return new ScanResult(new BasicVertex.Fake(pos), ScanAction.STOP);
    }

    @Nullable
    public Npc getNpc() {
        return builder.results().nearest();
    }

    @Override
    public String toString() {
        return new StringJoiner(",", "NpcVertex(", ")")
            .add("position=" + position)
            .add("action=" + action.pattern())
            .toString();
    }
}

package com.runemate.game.api.hybrid.cache.elements;

import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.region.*;
import java.util.*;
import java.util.concurrent.*;

public class CollisionGrid {
    private final int planes, width, height;
    private final com.runemate.game.api.hybrid.cache.elements.MapFileCacheExtract mapExtract;
    private final com.runemate.game.api.hybrid.cache.elements.LandscapeFileCacheExtract
        landscapeExtract;
    private int[][][] flags;
    private Map<Coordinate, Integer> regionBoundaryOverflowFlags;

    public CollisionGrid(
        int planes, int width, int height, MapFileCacheExtract mapExtract,
        LandscapeFileCacheExtract landscapeExtract
    ) {
        this.planes = planes;
        this.width = width;
        this.height = height;
        this.mapExtract = mapExtract;
        this.landscapeExtract = landscapeExtract;
    }

    private static int flipDirection(int dir) {
        return (dir + 4) & 0x7;
    }

    private static int xoff(int dir) {
        switch (dir) {
            case 0:
            case 6:
            case 7:
                return -1;
            case 1:
            case 5:
                return 0;
            case 2:
            case 3:
            case 4:
                return 1;
        }
        return 0;
    }

    private static int yoff(int dir) {
        switch (dir) {
            case 0:
            case 1:
            case 2:
                return 1;
            case 3:
            case 7:
                return 0;
            case 4:
            case 5:
            case 6:
                return -1;
        }
        return 0;
    }

    private static int getBase(boolean impassable, boolean penetrable) {
        return 1;//| (impassable ? 0b1000000000 : 0) | (penetrable ? 0b10000000000000000000000 : 0);
    }

    private void addClippingFlags(
        byte[][][] render_flags, int[][][] collision_flags,
        int[][][] underlay_ids, int[][][] overlay_ids, int planes,
        int width, int height
    ) {
        boolean underwater = false;//TODO
        if (!underwater) {
            for (int plane = 0; plane < planes; ++plane) {
                for (int regionX = 0; regionX < width; ++regionX) {
                    for (int regionY = 0; regionY < height; ++regionY) {
                        if ((render_flags[plane][regionX][regionY] & Region.RenderFlags.CLIPPED) ==
                            Region.RenderFlags.CLIPPED) {
                            int zidx = plane;
                            if ((
                                render_flags[1][regionX][regionY] &
                                    Region.RenderFlags.LOWER_OBJECTS_TO_OVERRIDE_CLIPPING
                            ) ==
                                Region.RenderFlags.LOWER_OBJECTS_TO_OVERRIDE_CLIPPING) {
                                zidx--;
                            }
                            if (zidx >= 0) {
                                addFlag(collision_flags, zidx, regionX, regionY,
                                    Region.CollisionFlags.BLOCKED_TILE
                                );
                            }
                        }
                        //Note/WARNING: This is a custom hack we apply to attempt to reduce the amount of redundant vertices that exist on planes 1-3
                        //In the future it should be moved into the getOutOfBoundVertices method located in the navigation graph generator.
                        /*if (plane > 0 && underlay_ids[plane][regionX][regionY] == 0&& overlay_ids[plane][regionX][regionY] == 0) {
                            addFlag(collision_flags, plane, regionX, regionY, Region.CollisionFlags.BLOCKED_TILE);
                        }*/
                    }
                }
            }
        }
    }

    private void addPrimaryObjectFlags(
        int[][][] flags, int plane, int regionX, int regionY,
        int width, int height, boolean impassible,
        boolean allow_range
    ) {
        int flag = Region.CollisionFlags.OBJECT_TILE;
        if (impassible) {
            flag |= Region.CollisionFlags.UNSTEPPABLE_OBJECT;
        }
        if (allow_range) {
            flag |= Region.CollisionFlags.RANGEABLE_OBJECT;
        }
        for (int currentX = regionX; currentX < regionX + width; ++currentX) {
            if (currentX >= 0 && currentX < flags[plane].length) {
                for (int currentY = regionY; currentY < regionY + height; ++currentY) {
                    if (currentY >= 0 && currentY < flags[plane][currentX].length) {
                        addFlag(flags, plane, currentX, currentY, flag);
                    }
                }
            }
        }
    }

    private void addFloorObjectFlag(int[][][] flags, int plane, int regionX, int regionY) {
        addFlag(flags, plane, regionX, regionY, Region.CollisionFlags.BLOCKING_FLOOR_OBJECT);
    }

    private void addBoundaryObjectFlags(
        int[][][] flags, int plane, int regionX, int regionY,
        int type, int orientation, boolean impassable,
        boolean penetrable
    ) {
        if (type == 0) {
            int dir = ((orientation << 1) - 1) & 0x7;
            addFlag(flags, plane, regionX, regionY, getBase(impassable, penetrable) << dir);
            addFlag(flags, plane, regionX + xoff(dir), regionY + yoff(dir),
                getBase(impassable, penetrable) << flipDirection(dir)
            );
        }
        if (type == 1 || type == 3) {
            int dir = (orientation << 1) & 0x7;
            addFlag(flags, plane, regionX, regionY, getBase(impassable, penetrable) << dir);
            addFlag(flags, plane, regionX + xoff(dir), regionY + yoff(dir),
                getBase(impassable, penetrable) << flipDirection(dir)
            );
        }
        if (type == 2) {
            int dir1 = ((orientation << 1) + 1) & 0x7;
            int dir2 = ((orientation << 1) - 1) & 0x7;
            addFlag(flags, plane, regionX, regionY,
                getBase(impassable, penetrable) << (dir1 | dir2)
            );
            addFlag(flags, plane, regionX + xoff(dir1), regionY + yoff(dir1),
                getBase(impassable, penetrable) << flipDirection(dir1)
            );
            addFlag(flags, plane, regionX + xoff(dir2), regionY + yoff(dir2),
                getBase(impassable, penetrable) << flipDirection(dir2)
            );
        }
        if (impassable) {
            if (type == 0) {
                if (orientation == 0) {
                    addFlag(flags, plane, regionX, regionY,
                        Region.CollisionFlags.RANGE_BLOCKING_WEST_BOUNDARY_OBJECT
                    );
                    addFlag(flags, plane, regionX - 1, regionY,
                        Region.CollisionFlags.RANGE_BLOCKING_EAST_BOUNDARY_OBJECT
                    );
                }
                if (orientation == 1) {
                    addFlag(flags, plane, regionX, regionY,
                        Region.CollisionFlags.RANGE_BLOCKING_NORTH_BOUNDARY_OBJECT
                    );
                    addFlag(flags, plane, regionX, regionY + 1,
                        Region.CollisionFlags.RANGE_BLOCKING_SOUTH_BOUNDARY_OBJECT
                    );
                }
                if (orientation == 2) {
                    addFlag(flags, plane, regionX, regionY,
                        Region.CollisionFlags.RANGE_BLOCKING_EAST_BOUNDARY_OBJECT
                    );
                    addFlag(flags, plane, regionX + 1, regionY,
                        Region.CollisionFlags.RANGE_BLOCKING_WEST_BOUNDARY_OBJECT
                    );
                }
                if (orientation == 3) {
                    addFlag(flags, plane, regionX, regionY,
                        Region.CollisionFlags.RANGE_BLOCKING_SOUTH_BOUNDARY_OBJECT
                    );
                    addFlag(flags, plane, regionX, regionY - 1,
                        Region.CollisionFlags.RANGE_BLOCKING_NORTH_BOUNDARY_OBJECT
                    );
                }
            }
            if (type == 1 || type == 3) {
                if (orientation == 0) {
                    addFlag(flags, plane, regionX, regionY,
                        Region.CollisionFlags.RANGE_BLOCKING_NORTH_WEST_BOUNDARY_OBJECT
                    );
                    addFlag(flags, plane, regionX - 1, regionY + 1,
                        Region.CollisionFlags.RANGE_BLOCKING_SOUTH_EAST_BOUNDARY_OBJECT
                    );
                }
                if (orientation == 1) {
                    addFlag(flags, plane, regionX, regionY,
                        Region.CollisionFlags.RANGE_BLOCKING_NORTH_EAST_BOUNDARY_OBJECT
                    );
                    addFlag(flags, plane, 1 + regionX, 1 + regionY,
                        Region.CollisionFlags.RANGE_BLOCKING_SOUTH_WEST_BOUNDARY_OBJECT
                    );
                }
                if (orientation == 2) {
                    addFlag(flags, plane, regionX, regionY,
                        Region.CollisionFlags.RANGE_BLOCKING_SOUTH_EAST_BOUNDARY_OBJECT
                    );
                    addFlag(flags, plane, regionX + 1, regionY - 1,
                        Region.CollisionFlags.RANGE_BLOCKING_NORTH_WEST_BOUNDARY_OBJECT
                    );
                }
                if (orientation == 3) {
                    addFlag(flags, plane, regionX, regionY,
                        Region.CollisionFlags.RANGE_BLOCKING_SOUTH_WEST_BOUNDARY_OBJECT
                    );
                    addFlag(flags, plane, regionX - 1, regionY - 1,
                        Region.CollisionFlags.RANGE_BLOCKING_NORTH_EAST_BOUNDARY_OBJECT
                    );
                }
            }
            if (type == 2) {
                if (orientation == 0) {
                    addFlag(flags, plane, regionX, regionY,
                        Region.CollisionFlags.RANGE_BLOCKING_WEST_BOUNDARY_OBJECT |
                            Region.CollisionFlags.RANGE_BLOCKING_NORTH_BOUNDARY_OBJECT
                    );
                    addFlag(flags, plane, regionX - 1, regionY,
                        Region.CollisionFlags.RANGE_BLOCKING_EAST_BOUNDARY_OBJECT
                    );
                    addFlag(flags, plane, regionX, 1 + regionY,
                        Region.CollisionFlags.RANGE_BLOCKING_SOUTH_BOUNDARY_OBJECT
                    );
                }
                if (orientation == 1) {
                    addFlag(flags, plane, regionX, regionY,
                        Region.CollisionFlags.RANGE_BLOCKING_EAST_BOUNDARY_OBJECT |
                            Region.CollisionFlags.RANGE_BLOCKING_NORTH_BOUNDARY_OBJECT
                    );
                    addFlag(flags, plane, regionX, 1 + regionY,
                        Region.CollisionFlags.RANGE_BLOCKING_SOUTH_BOUNDARY_OBJECT
                    );
                    addFlag(flags, plane, 1 + regionX, regionY,
                        Region.CollisionFlags.RANGE_BLOCKING_WEST_BOUNDARY_OBJECT
                    );
                }
                if (orientation == 2) {
                    addFlag(flags, plane, regionX, regionY,
                        Region.CollisionFlags.RANGE_BLOCKING_SOUTH_BOUNDARY_OBJECT |
                            Region.CollisionFlags.RANGE_BLOCKING_EAST_BOUNDARY_OBJECT
                    );
                    addFlag(flags, plane, regionX + 1, regionY,
                        Region.CollisionFlags.RANGE_BLOCKING_WEST_BOUNDARY_OBJECT
                    );
                    addFlag(flags, plane, regionX, regionY - 1,
                        Region.CollisionFlags.RANGE_BLOCKING_NORTH_BOUNDARY_OBJECT
                    );
                }
                if (orientation == 3) {
                    addFlag(flags, plane, regionX, regionY,
                        Region.CollisionFlags.RANGE_BLOCKING_WEST_BOUNDARY_OBJECT |
                            Region.CollisionFlags.RANGE_BLOCKING_SOUTH_BOUNDARY_OBJECT
                    );
                    addFlag(flags, plane, regionX, regionY - 1,
                        Region.CollisionFlags.RANGE_BLOCKING_NORTH_BOUNDARY_OBJECT
                    );
                    addFlag(flags, plane, regionX - 1, regionY,
                        Region.CollisionFlags.RANGE_BLOCKING_EAST_BOUNDARY_OBJECT
                    );
                }
            }
        }
        if (penetrable) {
            if (type == 0) {
                if (orientation == 0) {
                    addFlag(flags, plane, regionX, regionY,
                        Region.CollisionFlags.RANGE_ALLOWING_WEST_BOUNDARY_OBJECT
                    );
                    addFlag(flags, plane, regionX - 1, regionY,
                        Region.CollisionFlags.RANGE_ALLOWING_EAST_BOUNDARY_OBJECT
                    );
                }
                if (orientation == 1) {
                    addFlag(flags, plane, regionX, regionY,
                        Region.CollisionFlags.RANGE_ALLOWING_NORTH_BOUNDARY_OBJECT
                    );
                    addFlag(flags, plane, regionX, regionY + 1,
                        Region.CollisionFlags.RANGE_ALLOWING_SOUTH_BOUNDARY_OBJECT
                    );
                }
                if (orientation == 2) {
                    addFlag(flags, plane, regionX, regionY,
                        Region.CollisionFlags.RANGE_ALLOWING_EAST_BOUNDARY_OBJECT
                    );
                    addFlag(flags, plane, regionX + 1, regionY,
                        Region.CollisionFlags.RANGE_ALLOWING_WEST_BOUNDARY_OBJECT
                    );
                }
                if (orientation == 3) {
                    addFlag(flags, plane, regionX, regionY,
                        Region.CollisionFlags.RANGE_ALLOWING_SOUTH_BOUNDARY_OBJECT
                    );
                    addFlag(flags, plane, regionX, regionY - 1,
                        Region.CollisionFlags.RANGE_ALLOWING_NORTH_BOUNDARY_OBJECT
                    );
                }
            }
            if (type == 1 || type == 3) {
                if (orientation == 0) {
                    addFlag(flags, plane, regionX, regionY,
                        Region.CollisionFlags.RANGE_ALLOWING_NORTH_WEST_BOUNDARY_OBJECT
                    );
                    addFlag(flags, plane, regionX - 1, regionY + 1,
                        Region.CollisionFlags.RANGE_ALLOWING_SOUTH_EAST_BOUNDARY_OBJECT
                    );
                }
                if (orientation == 1) {
                    addFlag(flags, plane, regionX, regionY,
                        Region.CollisionFlags.RANGE_ALLOWING_NORTH_EAST_BOUNDARY_OBJECT
                    );
                    addFlag(flags, plane, regionX + 1, regionY + 1,
                        Region.CollisionFlags.RANGE_ALLOWING_SOUTH_WEST_BOUNDARY_OBJECT
                    );
                }
                if (orientation == 2) {
                    addFlag(flags, plane, regionX, regionY,
                        Region.CollisionFlags.RANGE_ALLOWING_SOUTH_EAST_BOUNDARY_OBJECT
                    );
                    addFlag(flags, plane, regionX + 1, regionY - 1,
                        Region.CollisionFlags.RANGE_ALLOWING_NORTH_WEST_BOUNDARY_OBJECT
                    );
                }
                if (orientation == 3) {
                    addFlag(flags, plane, regionX, regionY,
                        Region.CollisionFlags.RANGE_ALLOWING_SOUTH_WEST_BOUNDARY_OBJECT
                    );
                    addFlag(flags, plane, regionX - 1, regionY - 1,
                        Region.CollisionFlags.RANGE_ALLOWING_NORTH_EAST_BOUNDARY_OBJECT
                    );
                }
            }
            if (type == 2) {
                if (orientation == 0) {
                    addFlag(flags, plane, regionX, regionY,
                        Region.CollisionFlags.RANGE_ALLOWING_WEST_BOUNDARY_OBJECT |
                            Region.CollisionFlags.RANGE_ALLOWING_NORTH_BOUNDARY_OBJECT
                    );
                    addFlag(flags, plane, regionX - 1, regionY,
                        Region.CollisionFlags.RANGE_ALLOWING_EAST_BOUNDARY_OBJECT
                    );
                    addFlag(flags, plane, regionX, regionY + 1,
                        Region.CollisionFlags.RANGE_ALLOWING_SOUTH_BOUNDARY_OBJECT
                    );
                }
                if (orientation == 1) {
                    addFlag(flags, plane, regionX, regionY,
                        Region.CollisionFlags.RANGE_ALLOWING_EAST_BOUNDARY_OBJECT |
                            Region.CollisionFlags.RANGE_ALLOWING_NORTH_BOUNDARY_OBJECT
                    );
                    addFlag(flags, plane, regionX, regionY + 1,
                        Region.CollisionFlags.RANGE_ALLOWING_SOUTH_BOUNDARY_OBJECT
                    );
                    addFlag(flags, plane, regionX + 1, regionY,
                        Region.CollisionFlags.RANGE_ALLOWING_WEST_BOUNDARY_OBJECT
                    );
                }
                if (orientation == 2) {
                    addFlag(flags, plane, regionX, regionY,
                        Region.CollisionFlags.RANGE_ALLOWING_SOUTH_BOUNDARY_OBJECT |
                            Region.CollisionFlags.RANGE_ALLOWING_EAST_BOUNDARY_OBJECT
                    );
                    addFlag(flags, plane, regionX + 1, regionY,
                        Region.CollisionFlags.RANGE_ALLOWING_WEST_BOUNDARY_OBJECT
                    );
                    addFlag(flags, plane, regionX, regionY - 1,
                        Region.CollisionFlags.RANGE_ALLOWING_NORTH_BOUNDARY_OBJECT
                    );
                }
                if (orientation == 3) {
                    addFlag(flags, plane, regionX, regionY,
                        Region.CollisionFlags.RANGE_ALLOWING_WEST_BOUNDARY_OBJECT |
                            Region.CollisionFlags.RANGE_ALLOWING_SOUTH_BOUNDARY_OBJECT
                    );
                    addFlag(flags, plane, regionX, regionY - 1,
                        Region.CollisionFlags.RANGE_ALLOWING_NORTH_BOUNDARY_OBJECT
                    );
                    addFlag(flags, plane, regionX - 1, regionY,
                        Region.CollisionFlags.RANGE_ALLOWING_EAST_BOUNDARY_OBJECT
                    );
                }
            }
        }
    }

    private void addFlag(int[][][] flags, int plane, int regionX, int regionY, int flag) {
        if (regionX >= 0 && regionX < width && regionY >= 0 && regionY < height) {
            flags[plane][regionX][regionY] |= flag;
        } else {
            Coordinate flagPosition = new Coordinate(regionX, regionY, plane);
            int overflow = regionBoundaryOverflowFlags.getOrDefault(flagPosition, 0);
            overflow |= flag;
            regionBoundaryOverflowFlags.put(flagPosition, overflow);
        }
    }

    private void removeFlag(int[][][] flags, int plane, int regionX, int regionY, int flag) {
        if (regionX >= 0 && regionX < width
            && regionY >= 0 && regionY < height) {
            flags[plane][regionX][regionY] &= ~flag;
        } else {
            Coordinate flagPosition = new Coordinate(regionX, regionY, plane);
            int overflow = regionBoundaryOverflowFlags.getOrDefault(flagPosition, 0);
            overflow &= ~flag;
            regionBoundaryOverflowFlags.put(flagPosition, overflow);
        }
    }

    public int[][][] getFlags() {
        if (flags == null) {
            regionBoundaryOverflowFlags = new ConcurrentHashMap<>();
            flags = generate();
        }
        return flags;
    }

    public Map<Coordinate, Integer> getRegionBoundaryOverflowFlags() {
        if (regionBoundaryOverflowFlags == null) {
            regionBoundaryOverflowFlags = new ConcurrentHashMap<>();
            flags = generate();
        }
        return regionBoundaryOverflowFlags;
    }

    public int getFlagAt(int plane, int x, int y) {
        return getFlags()[plane][x][y];
    }

    private int[][][] generate() {
        final int[][][] flags = new int[planes][width][height];
        final byte[][][] renderFlags = mapExtract.getRenderFlags();
        final int[][][] underlayIds = mapExtract.getUnderlayIds();
        final int[][][] overlayIds = mapExtract.getOverlayIds();
        for (final LandscapeObject object : landscapeExtract.objects()) {
            int regionX = object.regionX();
            int regionY = object.regionY();
            int plane = object.floor();
            if (regionX >= 0 && regionX < width
                && regionY >= 0 && regionY < height
                && plane >= 0 && plane < planes) {
                //Verified this behavior as of r578
                if ((
                    renderFlags[1][regionX][regionY] &
                        Region.RenderFlags.LOWER_OBJECTS_TO_OVERRIDE_CLIPPING
                ) ==
                    Region.RenderFlags.LOWER_OBJECTS_TO_OVERRIDE_CLIPPING) {
                    --plane;
                }
                if (plane >= 0) {
                    object.floor(plane);
                    //Floor is adjusted here after the bridge is checked
                    if (object.specializedType() >= 4 && object.specializedType() <= 8) {
                        continue;
                    }
                    if (object.specializedType() == 22) {
                        if (object.getCollisionType() == 1) {
                            addFloorObjectFlag(flags, plane, regionX, regionY);
                        }
                    } else if (object.specializedType() >= 0 && object.specializedType() <= 3) {
                        if (object.getCollisionType() != 0) {
                            addBoundaryObjectFlags(flags, plane, regionX, regionY,
                                object.specializedType(), object.orientation(), object.impassable(),
                                !object.impenetrable()
                            );
                        }
                    } else if (object.specializedType() >= 9) {
                        if (object.getCollisionType() != 0) {
                            addPrimaryObjectFlags(flags, plane, regionX, regionY, object.width(),
                                object.height(), object.impassable(), !object.impenetrable()
                            );
                        }
                    } else if (object.specializedType() != -1) {
                        throw new IllegalStateException(
                            "Unknown object type '" + object.specializedType() + "' at " +
                                object.mapX() + ',' + object.mapY() + ',' + object.floor() + " (" +
                                object.getClass().getName() + ')');
                    }
                }
            }
        }
        addClippingFlags(renderFlags, flags, underlayIds, overlayIds, planes, width, height);
        return flags;
    }
}

package com.runemate.game.api.hybrid.location.navigation.web.requirements;

import com.runemate.game.api.hybrid.local.*;
import java.io.*;
import lombok.*;

public class VarpbitsRequirement extends WebRequirement implements SerializableRequirement {
    private int varpbitIndex;
    private int minValue, maxValue;

    public VarpbitsRequirement(int varpbitIndex, int value) {
        this.varpbitIndex = varpbitIndex;
        this.minValue = value;
        this.maxValue = value;
    }

    public VarpbitsRequirement(int varpbitIndex, int minValue, int maxValue) {
        this.varpbitIndex = varpbitIndex;
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    public VarpbitsRequirement(int protocol, ObjectInput stream) {
        super(protocol, stream);
    }

    @Override
    public boolean isMet0() {
        Varbit varpbits = Varbits.load(varpbitIndex);
        if (varpbits != null) {
            int value = varpbits.getValue();
            return value >= minValue && value <= maxValue;
        }
        return false;
    }

    @Override
    public int hashCode() {
        int result = varpbitIndex;
        result = 31 * result + minValue;
        result = 31 * result + maxValue;
        return result;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o instanceof VarpbitsRequirement) {
            VarpbitsRequirement that = (VarpbitsRequirement) o;
            return varpbitIndex == that.varpbitIndex && minValue == that.minValue &&
                maxValue == that.maxValue;
        }
        return false;
    }

    @Override
    public String toString() {
        return "VarpbitsRequirement(" + varpbitIndex + ", minValue=" + minValue + ", maxValue=" +
            maxValue + ')';
    }

    public int getVarpbitIndex() {
        return varpbitIndex;
    }

    public int getMinValue() {
        return minValue;
    }

    public int getMaxValue() {
        return maxValue;
    }

    @Override
    public int getOpcode() {
        return 3;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean serialize(ObjectOutput stream) {
        stream.writeInt(varpbitIndex);
        stream.writeInt(minValue);
        stream.writeInt(maxValue);
        return true;
    }

    @SneakyThrows(IOException.class)
    @Override
    public boolean deserialize(int protocol, ObjectInput stream) {
        varpbitIndex = stream.readInt();
        minValue = stream.readInt();
        maxValue = stream.readInt();
        return true;
    }
}

package com.runemate.game.api.hybrid.cache.configs;

import com.runemate.game.api.hybrid.cache.materials.*;

public interface OverlayDefinition {
    int getId();

    Material getMaterial();

    /**
     * Gets the color that is used when there isn't an underlay to match it
     */
    //Color getOpaqueColor();

    /**
     * Gets the color that is used to merge with the underlays color
     */
    //Color getTranslucentColor();

    //boolean blendsWithUnderlay();
}

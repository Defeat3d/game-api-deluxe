package com.runemate.game.api.hybrid.queries.results;

import com.runemate.game.api.hybrid.local.hud.interfaces.*;
import java.util.*;
import java.util.concurrent.*;

public class SpriteItemQueryResults
    extends InteractableQueryResults<SpriteItem, SpriteItemQueryResults> {
    public SpriteItemQueryResults() {
        super(new HashSet<>(32));
    }

    public SpriteItemQueryResults(final Collection<? extends SpriteItem> results) {
        super(results);
    }

    public SpriteItemQueryResults(
        final Collection<? extends SpriteItem> results,
        ConcurrentMap<String, Object> cache
    ) {
        super(results, cache);
    }

    @Override
    protected SpriteItemQueryResults get() {
        return this;
    }

    /**
     * Calls sort(Comparator) with a Comparator that will sort the list by index (lowest first)
     */
    public final SpriteItemQueryResults sortByIndex() {
        return sort(Comparator.comparingDouble(SpriteItem::getIndex));
    }

    /**
     * Calls sort(Comparator) with a Comparator that will sort the list by quantity
     */
    public final SpriteItemQueryResults sortByQuantity(boolean highestFirst) {
        Comparator<SpriteItem> comparator = Comparator.comparingDouble(SpriteItem::getQuantity);
        if (highestFirst) {
            comparator = comparator.reversed();
        }
        return sort(comparator);
    }
}

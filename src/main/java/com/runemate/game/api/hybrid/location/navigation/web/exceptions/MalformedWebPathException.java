package com.runemate.game.api.hybrid.location.navigation.web.exceptions;

public class MalformedWebPathException extends RuntimeException {
    public MalformedWebPathException(String message) {
        super(message);
    }
}

package com.runemate.game.api.hybrid.util;

import java.util.*;
import java.util.function.*;

@FunctionalInterface
public interface QuadFunction<A, B, C, D, R> {
    R apply(A a, B b, C c, D d);

    default <V> QuadFunction<A, B, C, D, V> andThen(
        Function<? super R, ? extends V> after
    ) {
        Objects.requireNonNull(after);
        return (A a, B b, C c, D d) -> after.apply(apply(a, b, c, d));
    }
}

package com.runemate.game.api.hybrid.cache.elements;

import com.runemate.game.api.hybrid.cache.configs.*;
import com.runemate.game.api.hybrid.cache.materials.*;
import com.runemate.game.api.hybrid.util.*;
import com.runemate.game.cache.io.*;
import com.runemate.game.cache.item.*;
import java.awt.*;
import java.io.*;
import java.util.List;
import java.util.*;

public class CacheIdentityKit extends IncrementallyDecodedItem {
    private final int id;
    private final boolean rs3;
    private final int[] headModelIds = { -1, -1, -1, -1, -1 };
    private int bodyPartId = -1;
    private boolean nonSelectable = false;
    private int[] modelIds;
    private short[] originalColors;
    private short[] modifiedColors;
    private short[] originalMaterialIds;
    private short[] modifiedMaterialIds;

    public CacheIdentityKit(int id, boolean rs3) {
        this.id = id;
        this.rs3 = rs3;
    }

    @Override
    public String toString() {
        return "CacheIdentityKit{" + "id=" + id
            + ", bodyPartId=" + bodyPartId + ", headModelIds="
            + Arrays.toString(headModelIds) + ", modelIds="
            + Arrays.toString(modelIds) + ", originalColors="
            + Arrays.toString(originalColors) + ", modifiedColors="
            + Arrays.toString(modifiedColors) + ", originalMaterialIds="
            + Arrays.toString(originalMaterialIds) + ", modifiedMaterialIds="
            + Arrays.toString(modifiedMaterialIds) + '}';
    }

    @Override
    protected void decode(Js5InputStream stream, int opcode) throws IOException {
        if (opcode == 1) {
            this.bodyPartId = stream.readUnsignedByte();
        } else if (opcode == 2) {
            int length = stream.readUnsignedByte();
            this.modelIds = new int[length];
            for (int index = 0; index < length; ++index) {
                this.modelIds[index] =
                    rs3 ? stream.readDefaultableUnsignedSmart() : stream.readUnsignedShort();
            }
        } else if (opcode == 3) {
            this.nonSelectable = true;
        } else if (opcode == 40) {
            int length = stream.readUnsignedByte();
            this.originalColors = new short[length];
            this.modifiedColors = new short[length];
            for (int index = 0; index < length; ++index) {
                this.originalColors[index] = (short) stream.readUnsignedShort();
                this.modifiedColors[index] = (short) stream.readUnsignedShort();
            }
        } else if (opcode == 41) {
            int length = stream.readUnsignedByte();
            this.originalMaterialIds = new short[length];
            this.modifiedMaterialIds = new short[length];
            for (int index = 0; index < length; ++index) {
                this.originalMaterialIds[index] = (short) stream.readUnsignedShort();
                this.modifiedMaterialIds[index] = (short) stream.readUnsignedShort();
            }
        } else if (opcode == 44) {
            stream.readUnsignedShort();
        } else if (opcode == 45) {
            stream.readUnsignedShort();
        } else if (opcode >= 60 && opcode < 70) {
            this.headModelIds[opcode - 60] =
                rs3 ? stream.readDefaultableUnsignedSmart() : stream.readUnsignedShort();
        }
    }

    public IdentityKit extended() {
        return new Extended();
    }

    public class Extended implements IdentityKit {
        private Extended() {
        }

        @Override
        public int getId() {
            return id;
        }

        @Override
        public int getBodyPartId() {
            return bodyPartId;
        }

        @Override
        public List<Integer> getModelIds() {
            if (modelIds == null) {
                return Collections.emptyList();
            }
            ArrayList<Integer> list = new ArrayList<>();
            for (int modelId : modelIds) {
                if (modelId != -1) {
                    list.add(modelId);
                }
            }
            return list;
        }

        @Override
        public List<Integer> getHeadModelIds() {
            ArrayList<Integer> list = new ArrayList<>();
            for (int headModelId : headModelIds) {
                if (headModelId != -1) {
                    list.add(headModelId);
                }
            }
            return list;
        }

        @Override
        public Map<Color, Color> getColorSubstitutions() {
            if (originalColors == null) {
                return Collections.emptyMap();
            }
            Map<Color, Color> colorMapping = new HashMap<>(originalColors.length);
            for (int i = 0; i < originalColors.length; ++i) {
                colorMapping.put(
                    RSColors.fromHSV(originalColors[i]),
                    RSColors.fromHSV(modifiedColors[i])
                );
            }
            return colorMapping;
        }

        @Override
        public Map<Material, Material> getMaterialSubstitutions() {
            if (originalMaterialIds == null) {
                return Collections.emptyMap();
            }
            Map<Material, Material> materialMapping = new HashMap<>(originalMaterialIds.length);
            for (int i = 0; i < originalMaterialIds.length; ++i) {
                materialMapping.put(
                    Materials.load(originalMaterialIds[i]),
                    Materials.load(modifiedMaterialIds[i])
                );
            }
            return materialMapping;
        }

        private CacheIdentityKit getParser() {
            return CacheIdentityKit.this;
        }

        @Override
        public String toString() {
            return "CacheIdentityKit{"
                + "id=" + id
                + ", bodyPartId=" + bodyPartId
                + ", headModelIds=" + Arrays.toString(headModelIds)
                + ", modelIds=" + Arrays.toString(modelIds)
                + ", originalColors=" + Arrays.toString(originalColors)
                + ", modifiedColors=" + Arrays.toString(modifiedColors)
                + ", originalMaterialIds=" + Arrays.toString(originalMaterialIds)
                + ", modifiedMaterialIds=" + Arrays.toString(modifiedMaterialIds) + '}';
        }
    }
}

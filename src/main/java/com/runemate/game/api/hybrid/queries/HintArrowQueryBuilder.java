package com.runemate.game.api.hybrid.queries;

import com.runemate.game.api.hybrid.entities.*;
import com.runemate.game.api.hybrid.local.hud.*;
import com.runemate.game.api.hybrid.location.*;
import com.runemate.game.api.hybrid.queries.results.*;
import java.util.*;
import java.util.concurrent.*;

public class HintArrowQueryBuilder
    extends QueryBuilder<HintArrow, HintArrowQueryBuilder, HintArrowQueryResults> {
    private int[] types;
    private Actor[] targeting;
    //Copied from LocatableEntityQueryBuilder
    private Area[] within;
    private Coordinate[] on;

    public final HintArrowQueryBuilder types(int... types) {
        this.types = types;
        return get();
    }

    @Override
    public HintArrowQueryBuilder get() {
        return this;
    }

    @Override
    public Callable<List<? extends HintArrow>> getDefaultProvider() {
        return () -> HintArrows.getLoaded().asList();
    }

    @Override
    public boolean accepts(HintArrow argument) {
        boolean condition;
        if (types != null) {
            condition = false;
            int arg_type = argument.getType();
            for (int type : types) {
                if (arg_type == type) {
                    condition = true;
                    break;
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (targeting != null) {
            condition = false;
            Actor arg_target = argument.getTarget();
            for (Actor target : targeting) {
                if (arg_target != null ? arg_target.equals(target) : target == null) {
                    condition = true;
                    break;
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (on != null) {
            condition = false;
            Area area = argument.getArea();
            if (area != null) {
                for (final Coordinate coordinate : on) {
                    if (area.contains(coordinate)) {
                        condition = true;
                        break;
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        if (within != null) {
            Coordinate position = argument.getPosition();
            condition = false;
            if (position != null) {
                for (final Area area : within) {
                    if (area.contains(position)) {
                        condition = true;
                        break;
                    }
                }
            }
            if (!condition) {
                return false;
            }
        }
        return super.accepts(argument);
    }

    @Override
    protected HintArrowQueryResults results(
        Collection<? extends HintArrow> entries,
        ConcurrentMap<String, Object> cache
    ) {
        return new HintArrowQueryResults(entries, cache);
    }

    //Copied from LocatableEntityQueryBuilder

    public final HintArrowQueryBuilder targeting(Actor... targeting) {
        this.targeting = targeting;
        return get();
    }

    public final HintArrowQueryBuilder on(final Collection<Coordinate> positions) {
        return on(positions.toArray(new Coordinate[0]));
    }

    public final HintArrowQueryBuilder on(final Coordinate... positions) {
        this.on = positions;
        return get();
    }

    public final HintArrowQueryBuilder within(final Collection<Area> areas) {
        return within(areas.toArray(new Area[0]));
    }

    public final HintArrowQueryBuilder within(final Area... areas) {
        this.within = areas;
        return get();
    }
}

package com.runemate.ui.binding;

import com.runemate.game.api.hybrid.util.calculations.*;
import java.util.concurrent.*;
import javafx.beans.binding.*;
import javafx.collections.*;

public class RateBinding extends DoubleBinding {

    private final NumberExpression expression;
    private final NumberExpression clock;
    private final TimeUnit unit;

    public RateBinding(final NumberExpression expression, final NumberExpression clock, final TimeUnit unit) {
        this.expression = expression;
        this.clock = clock;
        this.unit = unit;
        super.bind(clock);
    }

    public RateBinding(final NumberExpression expression, final NumberExpression clock) {
        this(expression, clock, TimeUnit.HOURS);
    }

    @Override
    protected double computeValue() {
        return CommonMath.rate(unit, clock.longValue(), expression.doubleValue());
    }

    @Override
    public void dispose() {
        super.unbind(clock);
    }

    @Override
    public ObservableList<?> getDependencies() {
        return FXCollections.observableArrayList(clock, expression);
    }
}
